package stevens.software.cv.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import stevens.software.cv.model.About;
import stevens.software.cv.model.Experience;
import stevens.software.cv.service.AboutService;
import stevens.software.cv.service.ExperienceService;

import java.util.List;

@RestController
public class ExperienceController {

    @Autowired
    private ExperienceService experienceService;

    @GetMapping("/experience")
    public List<Experience> getAboutInformation(){
        return experienceService.getExperience();
    }
}
