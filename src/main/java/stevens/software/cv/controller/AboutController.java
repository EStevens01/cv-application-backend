package stevens.software.cv.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import stevens.software.cv.model.About;
import stevens.software.cv.service.AboutService;

@RestController
public class AboutController {

    @Autowired
    private AboutService aboutService;

    @GetMapping("/about")
    public About getAboutInformation(){
        return aboutService.getAboutInformation();
    }
}
