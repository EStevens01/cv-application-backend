package stevens.software.cv.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import stevens.software.cv.model.Qualification;
import stevens.software.cv.service.QualificationService;

import java.util.List;

@RestController
public class QualificationController {

    @Autowired
    private QualificationService qualificationService;

    @GetMapping("/qualifications")
    public List<Qualification> getEducation(){
        return qualificationService.getQualifications();
    }
}
