package stevens.software.cv.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import stevens.software.cv.model.About;
import stevens.software.cv.repository.AboutRepository;
import java.util.Optional;

@Service
public class AboutService{

    @Autowired
    private AboutRepository aboutRepository;

    public About getAboutInformation(){
        Optional<About> about = aboutRepository.findById(1);
        return about.orElse(null);
    }
}
