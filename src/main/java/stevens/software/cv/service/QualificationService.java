package stevens.software.cv.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import stevens.software.cv.model.Qualification;
import stevens.software.cv.repository.QualificationRepository;
import java.util.List;

@Service
public class QualificationService {

    @Autowired
    private QualificationRepository qualificationRepository;

    public List<Qualification> getQualifications(){
        return qualificationRepository.findAll();
    }
}
