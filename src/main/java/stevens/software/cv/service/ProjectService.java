package stevens.software.cv.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import stevens.software.cv.model.Project;
import stevens.software.cv.repository.ProjectRepository;
import java.util.List;

@Service
public class ProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    public List<Project> getProjects(){
        return projectRepository.findAll();
    }
}
