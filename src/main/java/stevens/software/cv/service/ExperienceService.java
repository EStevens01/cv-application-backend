package stevens.software.cv.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import stevens.software.cv.model.Experience;
import stevens.software.cv.repository.ExperienceRepository;
import java.util.List;

@Service
public class ExperienceService {

    @Autowired
    private ExperienceRepository experienceRepository;

    public List<Experience> getExperience(){
        List<Experience> h = experienceRepository.findAll();
        return h;
    }
}
