package stevens.software.cv.model;

import lombok.Builder;
import lombok.Data;
import javax.persistence.*;

@Data
@Entity
@Table
//@Builder
public class Qualification {

    @Id
    private Integer id;
    @Column
    private String dateFrom;
    @Column
    private String dateTo;
    @Column
    private String course;
    @Column
    private String description;
}
