package stevens.software.cv.model;

import lombok.Builder;
import lombok.Data;
import javax.persistence.*;

@Data
@Entity
@Table
//@Builder
public class About {

    @Id
    private Integer id;
    @Column
    private String name;
    @Column
    private String title;
    @Column
    private String about;
}
