package stevens.software.cv.model;

import lombok.Builder;
import lombok.Data;
import javax.persistence.*;

@Data
@Entity
@Table
//@Builder
public class Project {

    @Id
    @Column
    private Integer id;
    @Column
    private String title;
    @Column
    private String description;
}
