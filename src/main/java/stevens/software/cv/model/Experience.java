package stevens.software.cv.model;

import lombok.Builder;
import lombok.Data;
import javax.persistence.*;

@Data
@Entity
@Table
//@Builder
public class Experience {

    @Id
    private Integer id;
    @Column
    private String dateFrom;
    @Column
    private String dateTo;
    @Column
    private String title;
    @Column
    private String company;
//    @Column
//    private String description;
}
