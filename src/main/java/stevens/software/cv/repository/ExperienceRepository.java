package stevens.software.cv.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import stevens.software.cv.model.Experience;

@Repository
public interface ExperienceRepository extends JpaRepository<Experience, Integer> {
}
