package stevens.software.cv.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import stevens.software.cv.model.About;

@Repository
public interface AboutRepository extends JpaRepository<About, Integer> {
}
