package stevens.software.cv.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import stevens.software.cv.model.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer> {
}
