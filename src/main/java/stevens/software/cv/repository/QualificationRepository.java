package stevens.software.cv.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import stevens.software.cv.model.Qualification;

@Repository
public interface QualificationRepository extends JpaRepository<Qualification, Integer> {
}
