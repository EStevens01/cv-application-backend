package stevens.software.cv.util;

import com.google.gson.Gson;
import stevens.software.cv.model.About;
import stevens.software.cv.model.Experience;
import stevens.software.cv.model.Project;
import stevens.software.cv.model.Qualification;

import java.util.Collections;
import java.util.List;

public class TestUtils {

    public static String convertToJsonString(Object object) {
        Gson gson = new Gson();
        return gson.toJson(object);
    }

    public static About getAboutInformation() {
        About about = new About();
        about.setAbout("Some about information");

        return about;
    }

    public static List<Qualification> getQualifications() {
        Qualification qualification = new Qualification();
        qualification.setDateFrom("2019");
        qualification.setDateTo("2020");
        qualification.setCourse("Maths");
        qualification.setDescription("Maths school");

        return Collections.singletonList(qualification);
    }

    public static List<Experience> getExperience(){
        Experience experience = new Experience();
        experience.setDateFrom("2019");
        experience.setDateTo("2020");
        experience.setCompany("Some company");

        return Collections.singletonList(experience);
    }

    public static List<Project> getProjects(){
        Project project = new Project();
        project.setTitle("Project 1");
        project.setDescription("Project description");

        return Collections.singletonList(project);

    }
}
