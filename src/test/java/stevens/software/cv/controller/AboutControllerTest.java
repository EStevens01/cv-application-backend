package stevens.software.cv.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import stevens.software.cv.model.About;
import stevens.software.cv.repository.AboutRepository;
import stevens.software.cv.service.AboutService;
import stevens.software.cv.util.TestUtils;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class AboutControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AboutService aboutService;

    @MockBean
    private AboutRepository aboutRepository;

    @Test
    public void testGetAbout() throws Exception {
        About expected = TestUtils.getAboutInformation();
        when(aboutService.getAboutInformation()).thenReturn(expected);
        mvc.perform(get("/about"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(TestUtils.convertToJsonString(expected)));

    }
}
