package stevens.software.cv.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import stevens.software.cv.model.Experience;
import stevens.software.cv.repository.ExperienceRepository;
import stevens.software.cv.service.ExperienceService;
import stevens.software.cv.util.TestUtils;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ExperienceControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ExperienceService experienceService;

    @MockBean
    private ExperienceRepository experienceRepository;

    @Test
    public void testGetExperience() throws Exception {
        List<Experience> expected = TestUtils.getExperience();
        when(experienceService.getExperience()).thenReturn(expected);
        mvc.perform(get("/experience"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(TestUtils.convertToJsonString(expected)));
    }
}
