package stevens.software.cv.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import stevens.software.cv.model.Qualification;
import stevens.software.cv.repository.QualificationRepository;
import stevens.software.cv.service.QualificationService;
import stevens.software.cv.util.TestUtils;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class EducationControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private QualificationService qualificationService;

    @MockBean
    private QualificationRepository qualificationRepository;

    @Test
    public void testGetEducation() throws Exception {
        List<Qualification> expected = TestUtils.getQualifications();
        when(qualificationService.getQualifications()).thenReturn(expected);
        mvc.perform(get("/qualifications"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(TestUtils.convertToJsonString(expected)));
    }
}
