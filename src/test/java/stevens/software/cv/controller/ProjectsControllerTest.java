package stevens.software.cv.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import stevens.software.cv.model.Project;
import stevens.software.cv.repository.ProjectRepository;
import stevens.software.cv.service.ProjectService;
import stevens.software.cv.util.TestUtils;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ProjectsControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ProjectService projectService;

    @MockBean
    private ProjectRepository projectRepository;

    @Test
    public void testGetProjects() throws Exception {
        List<Project> expected = TestUtils.getProjects();
        when(projectService.getProjects()).thenReturn(expected);
        mvc.perform(get("/projects"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(TestUtils.convertToJsonString(expected)));
    }
}
