package stevens.software.cv.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import stevens.software.cv.model.Experience;
import stevens.software.cv.repository.ExperienceRepository;
import stevens.software.cv.util.TestUtils;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ExperienceServiceTest {

    @Mock
    private ExperienceRepository experienceRepository;

    @InjectMocks
    private ExperienceService experienceService;

    @Test
    public void testGetEducation(){
        List<Experience> expected = TestUtils.getExperience();
        when(experienceRepository.findAll()).thenReturn(expected);
        List<Experience> actual = experienceService.getExperience();
        assertEquals(expected, actual);
    }
}
