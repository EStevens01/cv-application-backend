package stevens.software.cv.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import stevens.software.cv.model.Qualification;
import stevens.software.cv.repository.QualificationRepository;
import stevens.software.cv.util.TestUtils;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class EducationServiceTest {

    @Mock
    private QualificationRepository qualificationRepository;

    @InjectMocks
    private QualificationService qualificationService;

    @Test
    public void testGetEducation(){
        List<Qualification> expected = TestUtils.getQualifications();
        when(qualificationRepository.findAll()).thenReturn(expected);
        List<Qualification> actual = qualificationService.getQualifications();
        assertEquals(expected, actual);
    }
}
