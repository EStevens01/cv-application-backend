package stevens.software.cv.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import stevens.software.cv.model.Project;
import stevens.software.cv.repository.ProjectRepository;
import stevens.software.cv.util.TestUtils;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ProjectsServiceTest {

    @Mock
    private ProjectRepository projectRepository;

    @InjectMocks
    private ProjectService projectService;

    @Test
    public void testGetProjects(){
        List<Project> expected = TestUtils.getProjects();
        when(projectRepository.findAll()).thenReturn(expected);
        List<Project> actual = projectService.getProjects();
        assertEquals(expected, actual);
    }
}
