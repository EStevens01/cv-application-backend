package stevens.software.cv.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import stevens.software.cv.model.About;
import stevens.software.cv.repository.AboutRepository;
import stevens.software.cv.util.TestUtils;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AboutServiceTest {

    @Mock
    private AboutRepository aboutRepository;

    @InjectMocks
    private AboutService aboutService;

    @Test
    public void testGetAbout(){
        About expected = TestUtils.getAboutInformation();
        when(aboutRepository.findById(1)).thenReturn(Optional.of(expected));
        About actual = aboutService.getAboutInformation();
        assertEquals(expected, actual);
    }
}

