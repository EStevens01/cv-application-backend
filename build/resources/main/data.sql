DROP TABLE IF EXISTS about;
DROP TABLE IF EXISTS experience;
DROP TABLE IF EXISTS project;
DROP TABLE IF EXISTS qualification;

CREATE TABLE about (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  name VARCHAR(250),
  title VARCHAR(250),
  about TEXT
);

INSERT INTO about (name, title, about) VALUES
  ('Esther Stevens', 'Software Engineer', 'Passionate developer with commercial experience developing microservices for an eCommerce site using Java and Spring Boot and 2 months of bootcamp training learning various different technologies. Able to self-manage on independent projects as well as work as part of a productive and collaborative team. Personal programming experience using Kotlin, Java and Android Studio, with a published Android application on the Google Play store. I am eager to learn new technologies and continue to expand my existing skills.');

CREATE TABLE experience (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  date_from VARCHAR(250) NOT NULL,
  date_to VARCHAR(250) NOT NULL,
  title VARCHAR(250) NOT NULL,
  company VARCHAR(250) NOT NULL
  );

INSERT INTO experience (date_from, date_to, title, company) VALUES
  ('Oct 2020', 'Present', 'Software Developer', 'Collinson Group'),
  ('Sep 2019', 'Sep 2020', 'Junior Software Developer', 'Mindscape'),
  ('July 2018', 'July 2019', 'Freelance Web Developer', 'Self');

CREATE TABLE project (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  title VARCHAR(250) NOT NULL,
  description TEXT NOT NULL
);
INSERT INTO project (title, description) VALUES
  ('Messaging Android Application', 'Currently working on a private messaging app using Kotlin and Android studio. Includes features such as sign up/log-in, friends management and one-to-one messaging. I have integrated with Firebase including using their Realtime NoSQL database.'),
  ('Note Taking Android Application', 'Note Taking application written in Java and Android. Allows users to create, delete, update and sort notes into categories. Connects to a custom Java, Spring Boot and PostgreSQL backend application using Retrofit. '),
  ('Published To-Do List Android Application', 'During my personal time I have taught myself how to develop Android applications using Kotlin on Android Studio. I have developed and published a To-Do List application on the Google Play store which allows usersto add, remove and update tasks, with added functionality such as drag & drop and swipe to delete using the RecyclerView. I have used core components of Android Architecture Components, such as LiveData, ViewModels and Room. This application is tested using Junit and Espresso.');

CREATE TABLE qualification (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  date_from VARCHAR(250) NOT NULL,
  date_to VARCHAR(250) NOT NULL,
  course VARCHAR(250) NOT NULL,
  description TEXT
);
INSERT INTO qualification (date_from, date_to, course, description) VALUES
('2017', '2019', 'Computing & IT and Psychology', 'The Open Univeristy - Currently postponing 3rd year (Gained a first for both year 1 and 2)'),
('2013', '2014', 'Games Development level 3', null);


